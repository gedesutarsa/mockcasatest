package id.co.gpscorp.financial.personal.mockcasatest
import cucumber.runtime.FeaturePathFeatureSupplier
import cucumber.runtime.io.ResourceLoader
import io.cucumber.junit.CucumberOptions
//import cucumber.runtime.RuntimeOptionsFactory

import cucumber.runtime.io.MultiLoader
import cucumber.runtime.model.FeatureLoader
import io.cucumber.core.options.RuntimeOptionsBuilder
import io.cucumber.junit.Cucumber

/**
 * run cucumber test
 */
class CucumberTestInvoker {


    fun runTest( clazz: Class<Any>) {
        val classLoader = clazz.classLoader
        val resourceLoader = MultiLoader(classLoader)
        val featureLoader = FeatureLoader(resourceLoader)
        RuntimeOptionsBuilder()
        val c =  Cucumber(clazz)

        //val runtimeOptionsFactory = RuntimeOptionsBuilder(clazz)
        //val featureSupplier = FeaturePathFeatureSupplier(featureLoader , null!)

    }
}