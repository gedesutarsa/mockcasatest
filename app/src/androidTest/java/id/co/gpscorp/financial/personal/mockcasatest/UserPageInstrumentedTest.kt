package id.co.gpscorp.financial.personal.mockcasatest

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SdkSuppress
import androidx.test.uiautomator.By
import org.junit.runner.RunWith
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import org.junit.Before
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import org.hamcrest.CoreMatchers.equalTo

import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Assert.assertThat
import org.junit.Test

@RunWith(AndroidJUnit4::class)
@SdkSuppress(minSdkVersion = 18)
class UserPageInstrumentedTest {
    private val BASIC_SAMPLE_PACKAGE = "id.co.gpscorp.financial.personal.mockcasa"
    private val LAUNCH_TIMEOUT = 5000
    private var mDevice: UiDevice? = null


    @Before
    fun startMainActivityFromHomeScreen() {
        // Initialize UiDevice instance
        mDevice = UiDevice.getInstance(getInstrumentation())
        if ( mDevice == null ) {
            println("Fail to access device")
            return
        }
        val swapDevice = mDevice!!
        // Start from the home screen
        swapDevice.pressHome()

        // Wait for launcher
        val launcherPackage = getLauncherPackageName()
        assertThat<String>(launcherPackage, notNullValue())
        swapDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), LAUNCH_TIMEOUT.toLong())

        // Launch the blueprint app
        val context = getApplicationContext<Context>()
        val intent = context.getPackageManager()
            .getLaunchIntentForPackage(BASIC_SAMPLE_PACKAGE)
        intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)    // Clear out any previous instances
        context.startActivity(intent)

        // Wait for the app to appear
        swapDevice.wait(
            Until.hasObject(By.pkg(BASIC_SAMPLE_PACKAGE).depth(0)),
            LAUNCH_TIMEOUT.toLong()
        )
    }

    @Test
    fun checkPreconditions() {
        assertThat<UiDevice>(mDevice, notNullValue())
    }


    @Test
    fun runInitTest () {
        val device = mDevice!!
        val txtName = device.findObject(By.res(BASIC_SAMPLE_PACKAGE , "txt_name"))
        val txtALamat = device.findObject(By.res(BASIC_SAMPLE_PACKAGE , "txt_address"))

        val nama = "Gede Sutarsa"
        val alamat = "Lingkungan Jadi babakan"
        txtName.text =nama
        txtALamat.text=alamat
        device.pressBack()
        val btn = device.findObject(By.res(BASIC_SAMPLE_PACKAGE , "btn_ok"))
        btn.click()
        val expectedSample = "$nama -@- $alamat"

        val changedText =  device.wait(Until.findObject(By.res(BASIC_SAMPLE_PACKAGE, "txt_sample_result")) , 500)
        assertThat<String>(changedText.text, org.hamcrest.CoreMatchers.`is`<String>(equalTo<String>(expectedSample)))




    }

    /**
     * Uses package manager to find the package name of the device launcher. Usually this package
     * is "com.android.launcher" but can be different at times. This is a generic solution which
     * works on all platforms.`
     */
    private fun getLauncherPackageName(): String {
        // Create launcher Intent
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)

        // Use PackageManager to get the launcher package name
        val pm = getApplicationContext<Context>().packageManager
        val resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY)
        return resolveInfo!!.activityInfo.packageName
    }

}