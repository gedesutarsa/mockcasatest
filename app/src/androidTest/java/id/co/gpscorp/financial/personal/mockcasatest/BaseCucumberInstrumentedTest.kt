package id.co.gpscorp.financial.personal.mockcasatest

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SdkSuppress
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * base class for android cucumber
 */
@RunWith(AndroidJUnit4::class)
@SdkSuppress(minSdkVersion = 18)
abstract class BaseCucumberInstrumentedTest {

    protected val LAUNCH_TIMEOUT = 5000
    protected var mDevice: UiDevice? = null


    abstract fun getBasePackageName() : String

    @Before
    fun startMainActivityFromHomeScreen() {
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        if ( mDevice == null ) {
            println("Fail to access device")
            return
        }
        val swapDevice = mDevice!!
        // Start from the home screen
        swapDevice.pressHome()

        // Wait for launcher
        val launcherPackage = getLauncherPackageName()
        Assert.assertThat<String>(launcherPackage, CoreMatchers.notNullValue())
        swapDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), LAUNCH_TIMEOUT.toLong())

        // Launch the blueprint app
        val context = ApplicationProvider.getApplicationContext<Context>()
        val intent = context.getPackageManager()
            .getLaunchIntentForPackage(getBasePackageName())
        intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)    // Clear out any previous instances
        context.startActivity(intent)

        // Wait for the app to appear
        swapDevice.wait(
            Until.hasObject(By.pkg(getBasePackageName()).depth(0)),
            LAUNCH_TIMEOUT.toLong()
        )
    }

    @Test
    abstract fun runTest()



    /**
     * Uses package manager to find the package name of the device launcher. Usually this package
     * is "com.android.launcher" but can be different at times. This is a generic solution which
     * works on all platforms.`
     */
    private fun getLauncherPackageName(): String {
        // Create launcher Intent
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)

        // Use PackageManager to get the launcher package name
        val pm = ApplicationProvider.getApplicationContext<Context>().packageManager
        val resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY)
        return resolveInfo!!.activityInfo.packageName
    }

}