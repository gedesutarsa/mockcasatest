package id.co.gpscorp.financial.personal.mockcasatest

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SdkSuppress
import androidx.test.uiautomator.By
import androidx.test.uiautomator.Until
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.runner.RunWith


class UserPageWithBaseInstrumentedTest  : BaseCucumberInstrumentedTest() {
    override fun runTest() {
        val basePackageName = this.getBasePackageName()
        val device = mDevice!!
        val txtName = device.findObject(By.res(basePackageName , "txt_name"))
        val txtALamat = device.findObject(By.res(basePackageName , "txt_address"))

        val nama = "Gede Sutarsa"
        val alamat = "Lingkungan Jadi babakan"
        txtName.text =nama
        txtALamat.text=alamat
        device.pressBack()
        val btn = device.findObject(By.res(basePackageName , "btn_ok"))
        btn.click()
        val expectedSample = "$nama -@- $alamat"

        val changedText =  device.wait(Until.findObject(By.res(basePackageName, "txt_sample_result")) , 500)
        Assert.assertThat<String>(
            changedText.text,
            CoreMatchers.`is`<String>(CoreMatchers.equalTo<String>(expectedSample))
        )
    }

    override fun getBasePackageName(): String  = "id.co.gpscorp.financial.personal.mockcasa"
}