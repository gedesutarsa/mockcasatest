package id.co.balabali.financial.qa.cucumber101.test;

import android.content.Context;
import android.content.Intent;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiDevice ;
import androidx.test.uiautomator.UiObject2;
import androidx.test.uiautomator.Until;

import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import static  androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static  androidx.test.core.app.ApplicationProvider.getApplicationContext;
import static  org.junit.Assert.assertThat;
import static  org.hamcrest.CoreMatchers.notNullValue;
import static   org.hamcrest.CoreMatchers.equalTo;

public class CasaAutomatorSteps {

    private static final String BASIC_SAMPLE_PACKAGE = "id.co.gpscorp.financial.personal.mockcasa";
    private static final int  LAUNCH_TIMEOUT = 5000 ;
    private UiDevice  mDevice ;

    @Before
    public void beforeTestTask() {
        this.mDevice = UiDevice.getInstance(getInstrumentation());
        this.mDevice.pressHome();
        final String launcherPackage = getLauncherPackageName();
        assertThat(launcherPackage , notNullValue());
        this.mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(10)) , LAUNCH_TIMEOUT);
        // Launch the blueprint app
        final Context context = getApplicationContext();
        final Intent intent = context.getPackageManager().getLaunchIntentForPackage(BASIC_SAMPLE_PACKAGE);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK) ;
        context.startActivity(intent);
        this.mDevice.wait(Until.hasObject(By.pkg(BASIC_SAMPLE_PACKAGE).depth(0)) , LAUNCH_TIMEOUT);

    }


    @Given("^user at StartActivity$")
    public void user_at_mainActivity() {




    }

    @Then("^for user , value is \"(.*)\"$")
    public void userEnterUsername( String username ) {
        UiObject2 txtName = this.mDevice.findObject(By.res(BASIC_SAMPLE_PACKAGE , "txt_name")) ;
        txtName.setText(username);
        //mDevice.pressBack();
    }

    @And("^for address,value is \"(.*)\"$")
    public void userEnterAddress( String address ) {
        UiObject2 txtALamat = mDevice.findObject(By.res(BASIC_SAMPLE_PACKAGE , "txt_address"));
        txtALamat.setText(address);
        mDevice.pressBack();
    }

    @Then("^user click button$")
    public void clickButton () {
        
        UiObject2 btn = mDevice.findObject(By.res(BASIC_SAMPLE_PACKAGE , "btn_ok"));
        btn.click();
    }

    @Then("^user see result: \"(.*)\"$")
    public void userShowResult( String reslt ) {
        UiObject2 changedText =  mDevice.wait(Until.findObject(By.res(BASIC_SAMPLE_PACKAGE, "txt_sample_result")) , 500);
        assertThat(changedText.getText(), org.hamcrest.CoreMatchers.is(equalTo(reslt)));
    }






    private String getLauncherPackageName() {
        final Intent intent = new Intent(Intent.ACTION_MAIN) ;
        intent.addCategory(Intent.CATEGORY_HOME);
        final ResolveInfo resolveInfo =  getApplicationContext().getPackageManager().resolveActivity(intent,PackageManager.MATCH_DEFAULT_ONLY );
        return resolveInfo.activityInfo.packageName;
    }
}
