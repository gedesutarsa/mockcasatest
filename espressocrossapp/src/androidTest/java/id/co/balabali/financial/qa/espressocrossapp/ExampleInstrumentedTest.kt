package id.co.balabali.financial.qa.espressocrossapp

import android.app.Activity
import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    private val ACTIVITY_TO_BE_TESTED = "id.co.gpscorp.financial.personal.signedcasa.UserInfoActivity"

    var activity: Activity? = null
    @Test
    fun useAppContext() {
        // Context of the app under test.
        //val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        //assertEquals("id.co.balabali.financial.qa.espressocrossapp", appContext.packageName)
        assertNotNull(this.activity)
        val act = this.activity!!
        val txtRealName = act.findViewById<EditText>(-1000054)
        txtRealName.setText("Test Name")
        val btn = act.findViewById<Button>(-1000003)
        btn.performClick()
        val txtResult = act.findViewById<TextView>(-1000202)
        val rslt =  txtResult.text
        println(rslt)
    }


    @Before
    fun setup() {
        var activityClass: Class<*>? = null
        try {
            activityClass = Class.forName(ACTIVITY_TO_BE_TESTED)
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
            return
        }

        val instrumentation = InstrumentationRegistry.getInstrumentation()
        instrumentation.setInTouchMode(true)

        val targetPackage = instrumentation.targetContext.packageName
        val startIntent = Intent(Intent.ACTION_MAIN)
        startIntent.setClassName(targetPackage, activityClass.name)
        startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        this.activity = instrumentation.startActivitySync(startIntent)
        instrumentation.waitForIdleSync()
        //act.findViewById()
    }
}
